// Story 8

$("#keyword").keyup( function() {
    var ketikan = $("#keyword").val(); // Return the value attribute: argumen untuk ke googleapis
    console.log(ketikan);
    //panggil dengan teknik ajax
    $.ajax({ 
        url: '/data?q=' + ketikan, //memanggil ke googleapi dengan paremeter ketikan
        success: function(data) { // data berisi jason
            var array_items = data.items;
            console.log(array_items);
            $("#daftar_isi").empty();
            var bookBox;
            for (i = 0; i < array_items.length; i++) {
                bookBox = '';
                // insert thumbnail
                var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                // insert title
                var judul = '<b>' + array_items[i].volumeInfo.title + '</b>';
                // insert author
                var authors = array_items[i].volumeInfo.authors;
                // insert link
                var link = array_items[i].id;
                // append result bookBox
                $("#daftar_isi").append('<tr><td><img src =' + gambar + '></td><td>' + judul + '</td><td>' + authors + '</td><td><a href= https://play.google.com/store/books/details?id=' + link + '&source=gbs_api class="btn btn-outline-dark pl-2 pr-2"> Read more </a>');
            }
        }
    });
});