"""ppw_story URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  patsh('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from ppw_story1.views import index
from ppw_story4.views import courses
from ppw_story6.views import kegiatan
from ppw_story8.views import fungsi_data
from ppw_story9 import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('ppw_story4.urls')),
    path('index', include('ppw_story1.urls')),
    path('kegiatan/', include('ppw_story6.urls')),
    path('accordion/', include('ppw_story7.urls')),
    path('bukubuku/', include('ppw_story8.urls')),
    path('data/', fungsi_data),
    path('register/', views.registerPage, name="register"),
	path('login/', views.loginPage, name="login"),
	path('logout/', views.logoutUser, name="logout"),
]
