# most of the codes are referenced from : 
# https://github.com/divanov11/crash-course-CRM/tree/Part--14-User-Registration-%26-Login-Authentication/crm1_v14_registration_login

from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']