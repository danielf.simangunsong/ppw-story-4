from django.test import Client, TestCase
from django.urls import resolve, reverse
from ppw_story9 import views
from http import HTTPStatus
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

# Create your tests here.
class Testing(TestCase):

    def test_register_url(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code, 200)

    def test_register_func(self):
        response = resolve('/register/')
        self.assertEquals(response.func, views.registerPage)

    def test_register_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'home/register.html')

    def test_login_url(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)

    def test_login_func(self):
        response = resolve('/login/')
        self.assertEquals(response.func, views.loginPage)

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'home/login.html')

    def test_logout_url(self):
        response = Client().get('/logout/')
        self.assertEquals(response.status_code, 302)

    def test_logout_func(self):
        response = resolve('/logout/')
        self.assertEquals(response.func, views.logoutUser)

    def test_user_added(self):
        User.objects.create(username="name", email="name@anemail.com")
        jumlah = User.objects.all().count()
        self.assertEquals(1, jumlah)
        nama = User.objects.get(username="name")
        self.assertEquals("name", str(nama))

    def test_login(self):
        self.credentials = {
            'username' : 'iniadalahnama',
            'password' : 'entahapakekbuat123'
        }
        User.objects.create_user(**self.credentials)
        response = self.client.post('/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)

    def test_logout(self):
        self.credentials = {
            'username' : 'name',
            'password' : 'name12345'
        }
        User.objects.create_user(**self.credentials)
        self.client.login(username='name', password='name12345')
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)

    def test_wrong_login(self):
        self.credentials = {
            'username' : 'name',
            'password' : 'name12345'
        }
        User.objects.create_user(**self.credentials)
        wrong_input = {
            'username' : 'name',
            'password' : 'namelagi'
        }
        response = self.client.post('/login/', wrong_input, follow=True)
        self.assertFalse(response.context['user'].is_active)

    def test_signup(self):
        data_signup = {
            'username' : 'name',
            'email' : 'name@anemail.com',
            'password1' : 'name12345',
            'password2' : 'name12345'
        }
        response = self.client.post(reverse('register'), data_signup)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(User.objects.all().count(), 1)

    