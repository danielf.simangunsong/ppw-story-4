from django.urls import path

from . import views

app_name = 'ppw_story4'

urlpatterns = [
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('about', views.about, name='about'),
    path('feedig', views.feedig, name='feedig'),
    path('resume', views.resume, name='resume'),
    path('contacts', views.contacts, name='contacts'),
    path('forms', views.forms, name='forms'),
    path('course', views.courses, name='course'),
    path('courses/<delete_course>', views.delete, name='courses'),
    path('detail/<detail_course>', views.detail, name="detail")
]