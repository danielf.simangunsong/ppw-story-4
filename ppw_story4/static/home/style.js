// Story 7

$(".item-header").click(function(){
    $(".accordion-item").removeClass("active");
    $(this).parent().addClass("active");
});

$(".reorder-up").click(function(){
var $current = $(this).closest('.accordion-item')
var $previous = $current.prev('.accordion-item');
if($previous.length !== 0){
$current.insertBefore($previous);
}
return false;
});

$(".reorder-down").click(function(){
var $current = $(this).closest('.accordion-item')
var $next = $current.next('.accordion-item');
if($next.length !== 0){
    $current.insertAfter($next);
}
return false;
});


