from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    course_Name = models.CharField(max_length=200)
    lecturer = models.CharField(max_length=200)
    credits = models.CharField(max_length=200)
    course_Description = models.CharField(max_length=200)
    TERMS = (
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
    )
    term = models.CharField(max_length=200, choices=TERMS, default='Gasal 2020/2021')
    room = models.CharField(max_length=200)
    featured = models.BooleanField(default=True)

    def __str__(self):
        return self.course_Name