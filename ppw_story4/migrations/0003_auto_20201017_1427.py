# Generated by Django 3.1.2 on 2020-10-17 07:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ppw_story4', '0002_auto_20201015_2222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matakuliah',
            name='term',
            field=models.CharField(choices=[('Gasal 2020/2021', 'Gasal 2020/2021'), ('Genap 2019/2020', 'Gasal 2019/2020'), ('Gasal 2020/2021', 'Gasal 2020/2021'), ('Gasal 2020/2021', 'Gasal 2020/2021')], default='Gasal 2020/2021', max_length=200),
        ),
    ]
