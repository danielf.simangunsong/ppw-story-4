from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import MataKuliahForm
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import DeleteView
from django.shortcuts import get_object_or_404

from django.contrib.auth.decorators import login_required


# Create your views here.
def home(request):
    return render(request, 'home/home.html')

def about(request):
    return render(request, 'home/about.html')

def feedig(request):
    return render(request, 'home/ig-feed.html')

def resume(request):
    return render(request, 'home/resume.html')

def contacts(request):
    return render(request, 'home/contact.html')

@login_required(login_url='login')
def forms(request):
    form = MataKuliahForm(request.POST or None)
    if (form.is_valid()):
        form.save()
        return redirect('/course')
    context = {
        'form' : form
    }
    return render(request, 'home/form.html', context)

@login_required(login_url='login')
def courses(request):
    courses = MataKuliah.objects.all()
    context = {
        'list' : courses
    }
    return render(request, 'home/courses.html', context)

@login_required(login_url='login')
def delete(request, delete_course):
    MataKuliah.objects.filter(course_Name = delete_course).delete()
    return redirect('/course')

@login_required(login_url='login')
def detail(request, detail_course):
    course = MataKuliah.objects.get(course_Name=detail_course)
    context = {
        'course' : course
    }
    return render(request, 'home/detail.html', context)
