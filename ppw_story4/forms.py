from django.forms import ModelForm
from django import forms
from .models import MataKuliah

class MataKuliahForm(forms.ModelForm):
	class Meta:
		model = MataKuliah
		fields = (
		'course_Name', 
		'lecturer', 
		'credits', 
		'course_Description', 
		'term',
		'room'
		)

		widgets = {
			'course_Name': forms.TextInput(attrs={'class': 'form-control'}),
			'lecturer': forms.TextInput(attrs={'class': 'form-control'}),
			'credits': forms.TextInput(attrs={'class': 'form-control'}),
			'course_Description': forms.Textarea(attrs={'class': 'form-control'}),
			'term': forms.Select(attrs={'class': 'form-control'}),
			'room': forms.TextInput(attrs={'class': 'form-control'}),
		}