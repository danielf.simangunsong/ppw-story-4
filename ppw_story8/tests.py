from django.test import TestCase, Client
from django.urls import resolve
from . import views

class Testing(TestCase):
	def test_apakah_url_bukubuku_ada(self):
		response = Client().get('/bukubuku/')
		self.assertEquals(response.status_code, 302)

	# def test_apakah_di_halaman_bukubuku_ada_templatenya(self):
	# 	response = Client().get('/bukubuku/')
	# 	self.assertTemplateUsed(response, 'home/bukubuku.html')
	
	def test_apakah_fungsi_view_bukubuku_digunakan(self):
		function = resolve('/bukubuku/')
		self.assertEqual(function.func, views.bukubuku)
	
	def test_pemanggilan_sesuai_keyword(self):
		response = Client().get('/data/?q=harry')
		self.assertEqual(response.status_code, 302)
 