from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='login')
def bukubuku(request):
    response = { }
    return render(request, 'home/bukubuku.html', response)

@login_required(login_url='login')
def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)