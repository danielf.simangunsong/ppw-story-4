from django.urls import path

from . import views

app_name = 'ppw_story8'

urlpatterns = [
    path('', views.bukubuku, name='bukubuku'),
]