from django.urls import path

from . import views

app_name = 'ppw_story7'

urlpatterns = [
    path('', views.accordion, name='accordion'),
]