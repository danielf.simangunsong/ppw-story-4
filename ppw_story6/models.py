from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    activity = models.CharField(max_length=200)

    def __str__(self):
        return self.activity

class Orang(models.Model):
    name = models.CharField(max_length=200)
    activity = models.ForeignKey(Kegiatan, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nama
