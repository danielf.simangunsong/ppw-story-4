from django.test import TestCase, Client

# Create your tests here.
class Testing(TestCase):
	def test_apakah_url_kegiatan_ada(self):
		response = Client().get('/kegiatan/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_di_halaman_kegiatan_ada_templatenya(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'home/kegiatan.html')

	def test_apakah_di_halaman_kegiatan_ada_text_BUKU_TAMU_dan_tombol_Kirim(self):
		response = Client().get('/kegiatan/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("My Activities", html_kembalian)
		self.assertIn("Add", html_kembalian)