from django.forms import ModelForm
from django import forms
from .models import Kegiatan
from .models import Orang

class KegiatanForm(forms.ModelForm):
	class Meta:
		model = Kegiatan
		fields = (
		'activity',
		)

		widgets = {
			'activity': forms.TextInput(attrs={'class': 'form-control'})
		}

class OrangForm(forms.ModelForm):
	class Meta:
		model = Orang
		fields = (
		'name',
		)

		widgets = {
			'name': forms.TextInput(attrs={'class': 'form-control'})
		}