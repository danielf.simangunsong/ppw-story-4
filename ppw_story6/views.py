from django.shortcuts import redirect, render
from .models import Kegiatan, Orang
from .forms import KegiatanForm, OrangForm

# Create your views here.

def kegiatan(request):
    form = KegiatanForm(request.POST or None)
    if (form.is_valid()):
        form.save()
        return redirect('/courses')
    context = {
        'form' : form
    }
    return render(request, 'home/kegiatan.html', context)

def rumah(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
    form = KegiatanForm()
    Anggota = Orang.objects.all()
    Kegiatan = Kegiatan.objects.all()
    
    context = {
        'form' : form,
        'kegiatan' : Kegiatan,
        'anggota' : Anggota
    }
    return render(request, 'home/kegiatan.html', context)
 
def tambah(request, namakegiatan):
    if request.method == 'POST':
        form = OrangForm(request.POST)
        if form.is_valid():
            Kegiatan = kegiatan.objects.get(nama_kegiatan = namakegiatan)
            instance = form.save(commit = False)
            instance.nama_kegiatan = Kegiatan
            instance.save()
            return redirect('/story6')
    form = OrangForm()
    context = {
        'form' : form,
    }
    return render(request, 'daftar_anggota.html',context)
 
def detail(request, namakegiatan):
    Anggota = anggota.objects.all()
    context = {
        'ANGGOTA' : Anggota,
        'KEGIATAN' : namakegiatan
    }
    return render(request, 'detail_anggota.html', context)