from django.urls import path

from . import views

app_name = 'ppw_story6'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
]