from django.urls import path

from . import views

app_name = 'ppw_story1'

urlpatterns = [
    path('', views.index, name='index')
]